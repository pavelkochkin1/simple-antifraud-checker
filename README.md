# simple-antifraud-checker

Service to check string similarity with Levenshtein Distance algorithm.

## Getting started

1. `git clone <repo>`
2. change dir to `src/main/docker/`
3. Start postgres service and antifraud API with `docker-compose up`
4. DONE. Service work on your host machine, port `8081`

## Example of available equests:
* POST `localhost:8081/users` with json {"username": <name of your service>, "password": <pass>} - for registration in API
* GET `localhost:8081/similarity/base?s1=Simankov&s2=Simkov` - to check just pair of strings
* GET `localhost:8081/similarity/stand-reg?name1=Pavel&name2=Pasha&surname1=Kochkin&surname2=Kochin&login1=kochkin27@gmail.com&login2=kochkin_2002@mail.ru` - to check 3 rows, for example: name, surname and login. Returning average similarity
