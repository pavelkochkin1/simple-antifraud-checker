package com.dataculture.antifraud.stringsimilarity;

import com.dataculture.antifraud.stringsimilarity.interfaces.StringSimilarity;

public class Levenshtein implements StringSimilarity {
    public static double similarity (final String s1, final String s2) {
        if (s1 == null || s2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }

        double maxLength = Double.max(s1.length(), s2.length());
        if (maxLength > 0) {
            return (maxLength - getLevenshteinDistance(s1.toLowerCase(), s2.toLowerCase())) / maxLength;
        }
        return 1.0;
    }

    private static int getLevenshteinDistance(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();

        int[][] T = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            T[i][0] = i;
        }
        for (int j = 1; j <= n; j++) {
            T[0][j] = j;
        }

        int cost;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                cost = s1.charAt(i - 1) == s2.charAt(j - 1) ? 0: 1;
                T[i][j] = Integer.min(Integer.min(T[i - 1][j] + 1, T[i][j - 1] + 1),
                        T[i - 1][j - 1] + cost);
            }
        }

        return T[m][n];
    }
}
