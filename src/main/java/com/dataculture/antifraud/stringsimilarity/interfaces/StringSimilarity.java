package com.dataculture.antifraud.stringsimilarity.interfaces;

public interface StringSimilarity {
    /**
     * Compute and return the metric distance.
     *
     * @param s1
     * @param s2
     * @return
     */
    static double similarity(String s1, String s2) {
        return 0;
    }
}
