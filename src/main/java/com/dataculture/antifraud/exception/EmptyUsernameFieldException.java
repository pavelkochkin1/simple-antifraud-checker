package com.dataculture.antifraud.exception;

public class EmptyUsernameFieldException extends Exception{
    public EmptyUsernameFieldException(String message) {
        super(message);
    }
}
