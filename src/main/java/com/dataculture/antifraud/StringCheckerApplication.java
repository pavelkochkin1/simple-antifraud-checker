package com.dataculture.antifraud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringCheckerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringCheckerApplication.class, args);
	}

}
