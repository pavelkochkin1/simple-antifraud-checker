package com.dataculture.antifraud.controller;

import com.dataculture.antifraud.stringsimilarity.Levenshtein;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/similarity")
public class SimilarityController {

    @GetMapping("/base")
    public ResponseEntity getSimilarityScore(@RequestParam String s1,
                                             @RequestParam String s2
    ) {
        try {
            return ResponseEntity.ok(Levenshtein.similarity(s1, s2));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Some problem with calculating similarity...");
        }
    }

    @GetMapping("/stand-reg")
    public ResponseEntity getMeanScore(@RequestParam String login1,
                                       @RequestParam String login2,
                                       @RequestParam String name1,
                                       @RequestParam String name2,
                                       @RequestParam String surname1,
                                       @RequestParam String surname2
                                       ) {
        try {
            double login_score = Levenshtein.similarity(login1, login2);
            double name_score = Levenshtein.similarity(name1, name2);
            double surname_score = Levenshtein.similarity(surname1, surname2);
            return ResponseEntity.ok((login_score+name_score+surname_score)/3);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Some problem with calculating similarity...");
        }
    }
}
