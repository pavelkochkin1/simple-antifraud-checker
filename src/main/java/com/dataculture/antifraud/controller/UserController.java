package com.dataculture.antifraud.controller;

import com.dataculture.antifraud.entity.UserEntity;
import com.dataculture.antifraud.exception.EmptyUsernameFieldException;
import com.dataculture.antifraud.exception.UserAlreadyExistException;
import com.dataculture.antifraud.exception.UserNotFoundException;
import com.dataculture.antifraud.repository.UserRepo;
import com.dataculture.antifraud.service.UserService;
import com.dataculture.antifraud.stringsimilarity.Levenshtein;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity registration(@RequestBody UserEntity user) {
        try {
            userService.registration(user);
            return ResponseEntity.ok("User successfully saved");
        } catch (UserAlreadyExistException | EmptyUsernameFieldException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Some problem with registration...");
        }
    }

    @GetMapping
    public ResponseEntity getOneUser(@RequestParam Long id) {
        try {
            return ResponseEntity.ok(userService.getOne(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Some problem with our service...");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.delete(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Some problem with our service...");
        }
    }
}