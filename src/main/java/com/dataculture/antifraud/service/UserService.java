package com.dataculture.antifraud.service;

import com.dataculture.antifraud.entity.UserEntity;
import com.dataculture.antifraud.exception.EmptyUsernameFieldException;
import com.dataculture.antifraud.exception.UserAlreadyExistException;
import com.dataculture.antifraud.exception.UserNotFoundException;
import com.dataculture.antifraud.model.User;
import com.dataculture.antifraud.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    public UserEntity registration(UserEntity user) throws UserAlreadyExistException, EmptyUsernameFieldException {
        if (userRepo.findByUsername(user.getUsername()) != null) {
            throw new UserAlreadyExistException("User with such username is already exists.");
        } else if (user.getUsername() == "") {
            throw new EmptyUsernameFieldException("Empty username field.");
        }
        return userRepo.save(user);
    }

    public User getOne(Long id) throws UserNotFoundException {
        try {
            UserEntity user = userRepo.findById(id).get();
            return User.toModel(user);
        } catch (NoSuchElementException e) {
            throw new UserNotFoundException("User not found.");
        }
    }

    public Long delete(Long id) throws UserNotFoundException {
        try {
            UserEntity user = userRepo.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new UserNotFoundException("User not found.");
        }
        userRepo.deleteById(id);
        return id;
    }
}
